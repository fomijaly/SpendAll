# SpendAll

- Pré-requis :
    - Avoir `npm` installé sur votre machine
    - Installer en global le CLI d’Angular `npm i -g @angular/cli` si ce n'est pas déjà le cas
    - OU vérifier la version CLI installée  `ng v`

### Créer un nouveau projet 
Créer une application avec la commande `ng new` + le nom du projet
- génère une nouvelle application Angular
- installe les dépendances du projet
- initialise un dépôt

### Installer un projet déjà existant
- Cloner le dépôt git avec la commande `git clone` + le lien ou la clé ssh du projet.
- Lancer la commande `npm i` pour installer les dépendances nécessaires

### Démarrer le serveur de développement 
- Lancer la commande : 
    - `ng serve` ou `ng serve --open` pour exécuter le projet.
    - `ng generate component component-name` pour générer un nouveau composant.